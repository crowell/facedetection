﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Runtime.InteropServices;

namespace faceDetect
{


    public partial class Form1 : Form
    {
        private Capture cap;
        private HaarCascade haar;
        private Bgr currentcolor;
        private int faceX;
        private int faceY;
        private MCvAvgComp lastFace;
        private int nCheck = -1;
        public Form1()
        {
            // passing 0 gets zeroth webcam
            cap = new Capture(0);
            // adjust path to find your xml
            haar = new HaarCascade("haarcascade_frontalface_alt2.xml");
            currentcolor = new Bgr(0, 255, 255);
            faceX = 640;
            faceY = 480;

            InitializeComponent();
        }
        private void cursorMove(Emgu.CV.Structure.MCvAvgComp face)
        {
            if ((face.rect.X > faceX))
            {
                if ((face.rect.X - faceX) > 15)
                {
                    Cursor.Position = new Point(Cursor.Position.X + 2 * (face.rect.X - faceX), Cursor.Position.Y);

                    currentcolor = new Bgr(0, 0, 255);
                    faceX = face.rect.X;
                }
            }
            else
            {
                if ((faceX - face.rect.X) > 15)
                {
                    Cursor.Position = new Point(Cursor.Position.X - (1366/640) * (faceX - face.rect.X), Cursor.Position.Y);
                    currentcolor = new Bgr(0, 255, 0);
                    faceX = face.rect.X;

                }
            }
            if (faceY < face.rect.Y)
            {
                if ((face.rect.Y - faceY) > 15)
                {
                    Cursor.Position = new Point(Cursor.Position.X, Cursor.Position.Y + (768/480) * (face.rect.Y - faceY));

                    currentcolor = new Bgr(255, 0, 0);
                    faceY = face.rect.Y;
                }
            }
            else
            {
                if ((faceY - face.rect.Y) > 15)
                {
                    Cursor.Position = new Point(Cursor.Position.X, Cursor.Position.Y + 2 * (face.rect.Y - faceY));

                    currentcolor = new Bgr(255, 0, 255);
                    faceY = face.rect.Y;
                }
            }
            nCheck = 1;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            using (Emgu.CV.Image<Bgr, Byte> nextFrame = cap.QueryFrame())
            {
                if (nextFrame != null)
                {
                    // there's only one channel (greyscale), hence the zero index
                    //var faces = nextFrame.DetectHaarCascade(haar)[0];
                    Emgu.CV.Image<Bgr, Byte> grayframe = nextFrame.Convert<Bgr, Byte>();
                    grayframe = grayframe.Flip(FLIP.HORIZONTAL);
                    var faces =
                            grayframe.DetectHaarCascade(
                                    haar, 1.4, 4,
                                    HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
                                    new Size(nextFrame.Width / 8, nextFrame.Height / 8)
                                    )[0];
                    int it = 0;
                    Emgu.CV.Image<Bgr, Byte> mirror = nextFrame.Flip(FLIP.HORIZONTAL);
                    foreach (var face in faces)
                    {
                          if (it!=0)
                        {
                         //       break; //only get the first face seen
                        }
                        //nextFrame = nextFrame.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);
                          it++;
                        mirror.Draw(face.rect, currentcolor, 3);

                        cursorMove(face);
                    }
                    if (faces.Length == 0 && nCheck != -1)
                    {
                        int xmed = lastFace.rect.X + lastFace.rect.Width / 2;
                        int ymed = lastFace.rect.Y - lastFace.rect.Height / 2;

                        //if (nextFrame.GetSum() > 256 * 2)
                        {
                            MouseEventArgs lMouseClick = new MouseEventArgs(MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 1);
                            OnMouseClick(lMouseClick);

                        }
                    }

                    pictureBox1.Image = mirror.ToBitmap();
                }
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
